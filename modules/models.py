from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from profiles.models import Profile


class Module(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now)

    # Calculate time spent on a module
    @property
    def time_spent(self):
        from assignments.models import Assignment

        time = 0
        for assignment in Assignment.objects.filter(module=self):
            time += assignment.time_spent

        return time

    # Return the assignments that belong to a given module
    @property
    def assignments(self):
        from assignments.models import Assignment

        return Assignment.objects.filter(module=self)

    # Share a module with a teacher
    def share(self, username):
        from accounts.models import Account

        try:
            user = User.objects.get(username=username)
            account = Account.objects.get(user=user)

            # Check that user is actually a teacher
            if account.account_type == 'TEACHER':
                account.modules.add(self)
        except:
            print('USER DOES NOT EXIST')

    def __str__(self):
        return self.name
