from django.urls import path
from . import views

urlpatterns = [
    path('validate', views.validate_task, name='validate_task'),
]