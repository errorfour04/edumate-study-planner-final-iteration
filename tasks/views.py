from datetime import datetime

from django.shortcuts import render
from django.utils.datastructures import MultiValueDictKeyError
from django.http import JsonResponse

from .models import Task

# Validation


def validate_task(request):
    errors = {
        'title': None,
        'description': None,
        'studytype': None,
        'target': None,
        'dependencies': None,
        'start': None,
        'end': None
    }

    title = str(request.POST['title']).strip()
    description = str(request.POST['description']).strip()
    try:
        study_type = str(request.POST['studytype']).upper().strip()
    except MultiValueDictKeyError:
        study_type = ''
    target = request.POST['target']
    if 'dependencies' in request.POST:
        dependencies = request.POST.getlist('dependencies', [])
    else:
        dependencies = []
    start_date = datetime.strptime(
        request.POST['start'], "%Y-%m-%d").replace(tzinfo=None)
    end_date = datetime.strptime(
        request.POST['end'], "%Y-%m-%d").replace(tzinfo=None)

    # Title
    if len(title) > 80:
        errors.update({'title': 'Title is too long (2 - 40 characters)'})
    elif len(title) < 2:
        errors.update({'title': 'Title is too short (2 - 40 characters)'})

    # Description
    if len(description) > 240:
        errors.update(
            {'description': 'Description cannot be longer than 240 characters'})

    # Study Type

    # Dependencies
    if len(dependencies) > 0:
        for dep in dependencies:
            task = Task.objects.get(pk=dep)

            # Check date
            if task.end_date.replace(tzinfo=None) > start_date:
                errors.update(
                    {'dependencies': 'Dependent tasks need to be end before this task is started'})

    # Start Date
    if start_date < datetime.now():
        errors.update({'start': "Can't start task before today"})

    # End Date
    if end_date < start_date:
        errors.update(
            {'end': 'Your end date needs to be after your start date'})

    return JsonResponse(errors)
