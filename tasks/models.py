from django.db import models
from datetime import datetime, timedelta
from constants.types import STUDY_TYPES
from assignments.models import Assignment


class Task(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    description = models.TextField(max_length=240, blank=True)
    study_type = models.CharField(max_length=50, choices=STUDY_TYPES)
    target = models.IntegerField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    amount_complete = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)
    time_spent = models.IntegerField(default=0)
    dependencies = models.ManyToManyField('Task', blank=True)

    # Get the completion of task as a percentage
    @property
    def completion(self):
        return int(100 * self.amount_complete / self.target) or 0

    # Determine whether or not the task should have been started
    @property
    def is_started(self):
        return self.start_date.replace(tzinfo=None) < datetime.now()

    # Determine whether or not a task is overdue
    @property
    def is_overdue(self):
        return (not self.completed) and (
            self.end_date.replace(tzinfo=None) < datetime.now()
        )

    # Tasks that are due in a week
    @property
    def is_upcoming(self):
        return ((self.end_date.replace(tzinfo=None) <
                 (datetime.now() + timedelta(days=7))) and
                (not self.completed) and
                (not self.is_overdue))

    # Whether task is complete, overdue, etc
    @property
    def status(self):
        if self.completed:
            return "Complete"
        if self.is_overdue:
            return "Overdue"
        if self.is_started or self.amount_complete > 0:
            return "Ongoing"

        return "Not Started"

    # Returns the notes associated with a particular task
    @property
    def notes(self):
        return Note.objects.filter(task=self)

    # Add a dependency
    def add_dependency(self, id):
        other_task = Task.objects.get(pk=id)

        if other_task.assignment == self.assignment:
            self.dependencies.add(other_task)

    def __str__(self):
        return self.title


class Note(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    text = models.TextField(max_length=240)

    def __str__(self):
        return self.text
