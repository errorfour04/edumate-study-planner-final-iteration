ASSIGNMENT_TYPES = (
    ('EXAM', 'Exam'),
    ('COURSEWORK', 'Coursework')
)

STUDY_TYPES = (
    ('PROGRAMMING', 'Programming'),
    ('READING', 'Reading'),
    ('WRITING', 'Writing')
)

ACCOUNT_TYPES = (
    ('STUDENT', 'Student'),
    ('TEACHER', 'Teacher')
)
