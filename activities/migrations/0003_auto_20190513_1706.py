# Generated by Django 2.1.7 on 2019-05-13 17:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0002_activitylogs'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activitylogs',
            name='activity_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='activities.Activity'),
        ),
    ]
