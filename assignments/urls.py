from django.urls import path
from . import views

urlpatterns = [
    path('<int:assignment_id>', views.assignment, name='assignment'),
    path('<int:assignment_id>/create_task',
         views.create_task, name='create_task'),
    path('<int:assignment_id>/delete',
         views.delete_assignment, name='delete_assignment'),
    path('<int:assignment_id>/add_note/<int:task_id>',
         views.add_note, name='add_note'),
    path('<int:assignment_id>/do_activity',
         views.do_activity, name='do_activity'),
    path('<int:assignment_id>/add_activity',
         views.add_activity, name='add_activity'),
    path('<int:assignment_id>/add_milestone',
         views.add_milestone, name='add_milestone'),
    path('<int:assignment_id>/delete_task',
         views.delete_task, name='delete_task'),
    path('<int:assignment_id>/delete_milestone',
         views.delete_milestone, name='delete_milestone'),
    path('<int:assignment_id>/delete_activity',
         views.delete_activity, name='delete_activity'),
    path('<int:assignment_id>/edit',
         views.edit_assignment, name='edit_assignment'),
    path('<int:assignment_id>/edit_milestone',
         views.edit_milestone, name='edit_milestone'),
    path('<int:assignment_id>/edit_task',
         views.edit_task, name='edit_task'),
    path('<int:assignment_id>/edit_activity',
         views.edit_activity, name='edit_activity'),
    # path('viewm/<mil_name>', views.view_milestone, name='view_milestone')
]
