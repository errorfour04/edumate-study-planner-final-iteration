from django.contrib import admin
from .models import Assignment


class AssignmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'assignment_type', 'weighting',
                    'deadline', 'module')
    list_display_links = ('id', 'title',)
    list_filter = ('assignment_type', 'module')
    list_editable = ('assignment_type',)

    list_per_page = 25


admin.site.register(Assignment, AssignmentAdmin)
