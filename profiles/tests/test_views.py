from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from profiles.models import Profile


class ProfileViewTest(TestCase):
    def setUp(self):
        # Setup run before every test method.
        user = User(first_name='John', last_name='Doe',
                    username='johndoe', password='drowssap')
        user.save()

        Profile.objects.create(account=user, title='Test Module')

    def tearDown(self):
        # Clean up run after every test method.
        pass

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('localhost:8000/profiles/1/')
        self.assertEqual(response.status_code, 200)

    # def test_view_url_accessible_by_name(self):
    #     response = self.client.get(reverse('profiles'))
    #     self.assertEqual(response.status_code, 200)
