from django.test import TestCase
from django.contrib.auth.models import User

from profiles.models import Profile


class ProfileTestClass(TestCase):
    def setUp(self):
        # Setup run before every test method.
        user = User(first_name='John', last_name='Doe',
                    username='johndoe', password='drowssap')
        user.save()

        Profile.objects.create(account=user, title='Test Module')

    def tearDown(self):
        # Clean up run after every test method.
        pass

    def test_account_label(self):
        profile = Profile.objects.get(id=1)
        field_label = profile._meta.get_field('account').verbose_name
        self.assertEquals(field_label, 'account')

    def test_title_label(self):
        profile = Profile.objects.get(id=1)
        field_label = profile._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'title')

    def test_title_max_length(self):
        profile = Profile.objects.get(id=1)
        max_length = profile._meta.get_field('title').max_length
        self.assertEquals(max_length, 99)
