from django.urls import path
from . import views

urlpatterns = [
    path('<int:user_id>', views.dashboard, name='dashboard'),
    path('create_assignment',
         views.create_assignment, name='create_assignment'),
    path('share_module', views.share_module, name='share_module'),
    path('delete_module', views.delete_module, name='delete_module'),
    path('rename_module', views.rename_module, name='rename_module')
]
