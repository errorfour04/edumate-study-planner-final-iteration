from datetime import datetime

from django.shortcuts import render
from django.http import JsonResponse

from .models import Task

# Validation
def validate_milestone(request):
    errors = {
        'name': None,
        'task': None,
        'date': None,
        'time': None
    }

    # boolean value to determine if deadline is ahead of tasks to complete deadline
    deadline_valid = True

    name = request.POST['name']
    got_tasks = request.POST.getlist('task')

    date = request.POST['date']
    time = request.POST['time']

    concat = date + ' ' + time
    format_date_time = datetime.strptime(concat, '%Y-%m-%d %H:%M')

    if len(str(name).strip()) > 80 or len(str(name).strip()) < 2:
        errors.update({'name':'The name should be between 2 and 80 characters'})

    # iterate through all tasks and check their deadlines
    for task_id in got_tasks:
        task_from_id = Task.objects.get(id=task_id)
        task_from_id.end_date = task_from_id.end_date.replace(tzinfo=None)

        if task_from_id.end_date.replace(tzinfo=None) > format_date_time:
            deadline_valid = False

    if not deadline_valid:
        errors.update({'time':"A milestone can't be completed before its tasks"})
        # errors.update({'date':' '})

    return JsonResponse(errors)
