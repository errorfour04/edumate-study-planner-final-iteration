// Hits the backend to validate form input
function validate(inputSelector) {
    const selector = $(inputSelector)
    selector.on("submit", function(e) {
        e.preventDefault();

        errorsPresent = false;

        const url = selector.attr("validation-url");

        // Make the AJAX request
        $.ajax({
            url: url,
            method: 'post',
            data: selector.serialize(),
            dataType: "json",
            success: function(errors) {
                // Clear error messages
                $("*").removeClass("invalid-input");
                $("p").remove(".validation-message");

                for (i = 0; i < Object.keys(errors).length; i++) {
                    // Display errors
                    if (errors[Object.keys(errors)[i]]) {
                        errorsPresent = true;

                        const element = $(`[name="${Object.keys(errors)[i]}"]`);
                        element.addClass("invalid-input");

                        // Write Message
                        const errorMessage = `<p class="validation-message" \style="color: red; font-size: 0.8rem">${errors[Object.keys(errors)[i]]}</p>`
                        element.after(errorMessage);
                    }
                }

                // Submit the form if there are no errors
                if (errorsPresent == false) {
                    selector.unbind("submit");
                    selector.submit();
                }
            },
            fail: console.log("FAILED")
        })

    });
}