from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('',  include('pages.urls')),
    path('teacher/', include('accounts.urls')),
    path('profiles/', include('profiles.urls')),
    path('assignments/', include('assignments.urls')),
    path('tasks/', include('tasks.urls')),
    path('activities/', include('activities.urls')),
    path('milestones/', include('milestones.urls')),
    path('admin/', admin.site.urls),
]
